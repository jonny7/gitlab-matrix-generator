# GitLab Matrix Generator

I wrote this quick helper program to build a requirements matrix as part of my [Quetzal Bot](https://gitlab.com/jonny7/quetzal). Please feel free to use it or fork it. You can also look at Quetzal's CI/CD pipeline to see how to use it. Essentially it traverses all your `_test.go` files and looks for `//: reqId1,ReqId2`. It uses these to build the matrix of tests and what requirements they meet. It certainly could be improved, but if you just want a quick way to build these automatically it should do the trick.

